package cli

import (
	"fmt"
	"os"

	"wbtasks.com/internal/store"
)

var command_list string = `Command list:
list: show all orders in Store
help: get help
exit: exit
`

type CLI struct {
	DB *store.Store
}

func (c *CLI) Start() {
	fmt.Println("Service is working")
	fmt.Println(command_list)
	c.loopWorker()
}

func (cli *CLI) loopWorker() {
	var cmd string

	for {
		fmt.Print("> ")
		fmt.Scan(&cmd)

		switch cmd {
		case "list":
			fmt.Println("Orders:")
			for _, ord := range *cli.DB.Cache {
				fmt.Println("-", ord.OrderUID)
			}
		case "help":
			fmt.Println(command_list)
		case "exit":
			fmt.Println("Program closing...")
			os.Exit(0)
		default:
			fmt.Println("Command not found")
			fmt.Println(command_list)
		}
	}
}

func New(db *store.Store) *CLI {
	return &CLI{
		DB: db,
	}
}
