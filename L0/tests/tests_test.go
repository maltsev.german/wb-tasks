package tests

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"testing"
	"time"

	nats "github.com/nats-io/nats.go"
	"github.com/stretchr/testify/assert"

	"wbtasks.com/internal/order"
)

var URL string = "0.0.0.0:4223"

func TestModel0(t *testing.T) {
	nc, err := nats.Connect(URL)
	assert.Nil(t, err)

	defer nc.Close()

	fcontent, err := os.ReadFile("model0.json")
	assert.Nil(t, err)

	_, err = nc.Request("orders", fcontent, time.Second)
	if err != nil && err != nats.ErrTimeout {
		assert.Nil(t, err)
	}

	orderExpected := order.Order{}
	err = json.NewDecoder(bytes.NewBuffer(fcontent)).Decode(&orderExpected)
	assert.Nil(t, err)

	order_uid := orderExpected.OrderUID
	time.Sleep(time.Duration(time.Second * 2))
	resp, err := http.Get(fmt.Sprintf("http://127.0.0.1:8080?order_uid=%s", order_uid))
	assert.Nil(t, err)

	orderReal := order.Order{}
	err = json.NewDecoder(resp.Body).Decode(&orderReal)
	assert.Nil(t, err)

	assert.Equal(t, orderExpected, orderReal)
}

func TestModel1(t *testing.T) {
	nc, err := nats.Connect(URL)
	assert.Nil(t, err)

	defer nc.Close()

	fcontent, err := os.ReadFile("model1.json")
	assert.Nil(t, err)

	_, err = nc.Request("orders", fcontent, time.Second)
	if err != nil && err != nats.ErrTimeout {
		assert.Nil(t, err)
	}

	orderExpected := order.Order{}
	err = json.NewDecoder(bytes.NewBuffer(fcontent)).Decode(&orderExpected)
	assert.Nil(t, err)

	order_uid := orderExpected.OrderUID
	time.Sleep(time.Duration(time.Second * 2))
	resp, err := http.Get(fmt.Sprintf("http://127.0.0.1:8080?order_uid=%s", order_uid))
	assert.Nil(t, err)

	orderReal := order.Order{}
	err = json.NewDecoder(resp.Body).Decode(&orderReal)
	assert.Nil(t, err)

	assert.Equal(t, orderExpected, orderReal)
}

func TestModel2(t *testing.T) {
	nc, err := nats.Connect(URL)
	assert.Nil(t, err)

	defer nc.Close()

	fcontent, err := os.ReadFile("model2.json")
	assert.Nil(t, err)

	_, err = nc.Request("orders", fcontent, time.Second)
	if err != nil && err != nats.ErrTimeout {
		assert.Nil(t, err)
	}

	orderExpected := order.Order{}
	err = json.NewDecoder(bytes.NewBuffer(fcontent)).Decode(&orderExpected)
	assert.Nil(t, err)

	order_uid := orderExpected.OrderUID
	time.Sleep(time.Duration(time.Second * 2))
	resp, err := http.Get(fmt.Sprintf("http://127.0.0.1:8080?order_uid=%s", order_uid))
	assert.Nil(t, err)

	orderReal := order.Order{}
	err = json.NewDecoder(resp.Body).Decode(&orderReal)
	assert.Nil(t, err)

	assert.Equal(t, orderExpected, orderReal)
}
