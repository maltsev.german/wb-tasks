package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
	"unicode"
)

func main() {
	// Если не хватает аргументов, принтуем еррор
	if len(os.Args) == 1 {
		fmt.Fprintln(os.Stderr, "no args")
		os.Exit(1)
	}

	for i := 1; i < len(os.Args); i++ {
		// делаем распаковку строки
		if res, err := unpackString(os.Args[i]); err != nil {
			fmt.Fprintln(os.Stderr, err)
			// если спешно, печатаем результат
		} else {
			fmt.Println(res)
		}
	}
}

// сама функция распаковки строки
func unpackString(s string) (res string, err error) {
	rns := []rune(s)

	// если передали пустую строку, то пустую строку и возвращаем
	if len(rns) == 0 {
		return
	}

	// если строка начинается с цифры, то это невалидная строка
	// возращаем соответствующую ошибку
	if unicode.IsDigit(rns[0]) {
		err = fmt.Errorf("invalid string: begin of string cannot be a digit")
		return
	}

	// repeatCount содержит количество повторений символа
	var repeatCount int
	i := 0
	// бежим по строке
	for i < len(rns) {
		switch {
		// если встерили цифру ...
		case unicode.IsDigit(rns[i]):
			// переводим строковое число в int и сдвигаем i в конец числа
			repeatCount, err = getRepeatCount(rns, &i)
			if err != nil {
				return
			}
			r := []rune(res)
			// если число повторров равно 0
			if repeatCount == 0 {
				// делаем слайс без последнего символа
				r = r[:len(r)-1]
			} else {
				// иначе добавляем нужное количество символов
				addedString := strings.Repeat(string(r[len(r)-1]), repeatCount-1)
				r = append(r, []rune(addedString)...)
			}
			res = string(r)
		// если был встречен символ слеша ...
		case rns[i] == '\\':
			i++
			// если один слеш стоит в конце строки, то возращаем ошибку
			if i >= len(rns) {
				err = fmt.Errorf("invalid string: end of string connot be \\")
				return
				// елси следующий символ - цифра или еще один слеш
			} else if unicode.IsDigit(rns[i]) || rns[i] == '\\' {
				// добавляем этот символ в строку-результат
				res = string(append([]rune(res), rns[i]))
				i++
				// иначе, если не цифра или слеш после слеша - ошибка
			} else {
				err = fmt.Errorf("invalid string: after '\\' can be '\\' or a digit")
				return
			}
		default:
			res = string(append([]rune(res), rns[i]))
			i++
		}
	}
	return
}

// просто из части строки вычлиняем число
func getRepeatCount(rns []rune, i *int) (repeatCount int, err error) {
	startOfNumberIndex := *i
	skipNumber(rns, i)
	repeatCount, err = strconv.Atoi(string(rns[startOfNumberIndex:*i]))

	return
}

// сдвигает i в позицию после числа в строке
func skipNumber(rns []rune, i *int) {
	for *i < len(rns) && unicode.IsDigit(rns[*i]) {
		*i++
	}
}
