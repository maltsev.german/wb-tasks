package main

import "time"

// Отводим каждому каналу время для получения данных
// если время истекло, и данные не поступили, переходим к слудующему каналу
// и так по кругу
func or(chans ...<-chan interface{}) <-chan interface{} {
	dur := time.Millisecond * time.Duration(5)
	timer := time.NewTimer(dur)

	for {
		for _, ch := range chans {
			select {
			case <-ch:
				return ch
			case <-timer.C:
				timer.Reset(dur)
				continue
			}
		}
	}
}
