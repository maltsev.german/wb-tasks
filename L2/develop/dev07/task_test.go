package main

import (
	"fmt"
	"testing"
	"time"
)

func TestOr(t *testing.T) {
	n := time.Now()
	<-or(
		tFunc(2*time.Hour),
		tFunc(5*time.Minute),
		tFunc(1*time.Second),
		tFunc(1*time.Hour),
		tFunc(900*time.Millisecond),
	)

	fmt.Println(time.Since(n))
}

func tFunc(after time.Duration) <-chan interface{} {
	c := make(chan interface{})
	go func() {
		defer close(c)
		time.Sleep(after)
	}()
	return c
}
