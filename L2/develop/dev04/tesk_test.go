package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestIsAnagram(t *testing.T) {
	s1 := "fsd"
	s2 := "sfd"

	assert.True(t, isAnagram(&s1, &s2))
	assert.True(t, isAnagram(&s2, &s1))

	s1 = "Привет"
	s2 = "ветПир"

	assert.True(t, isAnagram(&s1, &s2))
	assert.True(t, isAnagram(&s2, &s1))

	s1 = "Привет"
	s2 = "вет Пир"

	assert.False(t, isAnagram(&s1, &s2))
	assert.False(t, isAnagram(&s2, &s1))

	s1 = "пятак"
	s2 = "пятка"

	assert.True(t, isAnagram(&s1, &s2))
	assert.True(t, isAnagram(&s2, &s1))
}

func TestGetAnagramsMap(t *testing.T) {
	sl := []string{"пятак", "пятка", "листок", "тяпка", "столик", "слиток", "герой", "ройге", "Пилот", "полит", "лопит", "лопит"}
	res := getAnagramsMap(sl)
	mustBe := map[string][]string{
		"герой":  {"герой", "ройге"},
		"листок": {"листок", "слиток", "столик"},
		"пилот":  {"лопит", "пилот", "полит"},
		"пятак":  {"пятак", "пятка", "тяпка"},
	}

	assert.Equal(t, mustBe, res)
}
