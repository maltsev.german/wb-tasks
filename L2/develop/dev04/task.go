package main

import (
	"reflect"
	"sort"
	"strings"
)

func getAnagramsMap(dict []string) (anagrams map[string][]string) {
	anagrams = map[string][]string{}
	wasAdded := false

	// каждое слово в словаре ...
	for i := range dict {
		// переводим в нижний регистр
		word := strings.ToLower(dict[i])
		wasAdded = false
		// ищем анаграму в мапе с анограмами
		for key, anagrms := range anagrams {
			// если нашли
			if isAnagram(&key, &word) && !isInSlice(anagrms, &word) {
				// добавляем в мапу
				anagrams[key] = append(anagrms, word)
				wasAdded = true
				break
			}
		}
		// если слово не было добавлено в мапу, значит, оно не является анограмой для каждого ключа мапы
		// анаграм. Используем его в качестве ключа для других слов, которые будут анаграмами этого слова
		if !wasAdded {
			anagrams[word] = []string{word}
		}
	}

	removeOneLenSlices(anagrams)
	sortSlices(anagrams)

	return
}

// проверяет, является ли строка s2 анаграмой строки s1
func isAnagram(s1, s2 *string) (answer bool) {
	s1Runes := []rune(*s1)
	s2Runes := []rune(*s2)

	sort.Slice(s1Runes, func(i, j int) bool {
		return s1Runes[i] > s1Runes[j]
	})

	sort.Slice(s2Runes, func(i, j int) bool {
		return s2Runes[i] > s2Runes[j]
	})

	answer = reflect.DeepEqual(&s1Runes, &s2Runes)

	return
}

// удаляет из мапы ключи со значениями слайсов длиной 1
func removeOneLenSlices(m map[string][]string) {
	for k, slice := range m {
		if len(slice) == 1 {
			delete(m, k)
		}
	}
}

// сортирует каждый слайс строк в m
func sortSlices(m map[string][]string) {
	for k := range m {
		sort.Strings(m[k])
	}
}

// проверяет, содержится ли строка s в слайсе sl
func isInSlice(sl []string, s *string) bool {
	for i := range sl {
		if sl[i] == *s {
			return true
		}
	}
	return false
}
