package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"sort"
	"strings"
)

// Parameters ...
type Parameters struct {
	k int  // по столбцу
	n bool // сравнивать численные значения строк
	r bool // reverse
	u bool // без дубликатов
}

var parameters Parameters

// инициализируем аргументы
func init() {
	flag.IntVar(&parameters.k, "k", -1, "указание колонки для сортировки (слова в строке могут выступать в качестве колонок, по умолчанию разделитель — пробел)")
	flag.BoolVar(&parameters.n, "n", false, "сортировать по числовому значению")
	flag.BoolVar(&parameters.r, "r", false, "сортировать в обратном порядке")
	flag.BoolVar(&parameters.u, "u", false, "не выводить повторяющиеся строки")
}

func main() {
	flag.Parse()

	if len(os.Args) <= 1 {
		fmt.Fprintln(os.Stderr, "передайте файл в качестве аргумента")
		return
	}

	filepath := os.Args[len(os.Args)-1]
	// считываем все строки из файла в filelLines
	fileLines, err := readFileLines(filepath)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		return
	}

	if parameters.u && parameters.k > 0 {
		// если параметр u и k, то удаляем все строки, в которых строк меньше k
		fileLines = removeLinesWithWordsLessThan(fileLines, parameters.k)
	}

	if parameters.u {
		// если u, удаляем дубликаты строк
		fileLines = removeStringsDuplicates(fileLines)
	}

	if parameters.k <= 0 {
		if parameters.n {
			// если не k и n, сортируем строки по длине строк
			sortStringsByNumeric(fileLines)
		} else {
			// иначе просто сортируем строки
			sortLines(fileLines)
		}
	} else {
		if parameters.n {
			// если k и n, сортируем строки по длине k-х слов
			sortLinesByColumnByNumeric(fileLines, parameters.k)
		} else {
			// если не n и k, сортируем строки по k-ым словам
			sortLinesByColumn(fileLines, parameters.k)
		}
	}

	if parameters.r {
		// если r, реверсируем массив слайс со строками
		reverseStringSlice(fileLines)
	}

	for _, line := range fileLines {
		fmt.Println(line)
	}
}

// просто сортирует строки
func sortLines(lines []string) {
	sort.Slice(lines, func(i, j int) bool {
		return strings.Compare(lines[i], lines[j]) < 0
	})
}

// сортирует строки по columnNumber столбцу
func sortLinesByColumn(strs []string, columnNumber int) {
	sort.Slice(strs, func(i, j int) bool {
		words1 := strings.Split(strings.TrimSpace(strs[i]), " ")
		words2 := strings.Split(strings.TrimSpace(strs[j]), " ")

		if len(words1) < columnNumber && len(words2) < columnNumber {
			return strings.Compare(strs[i], strs[j]) < 0
		} else if len(words1) < columnNumber {
			return true
		} else if len(words2) < columnNumber {
			return false
		}

		return strings.Compare(words1[columnNumber-1], words2[columnNumber-1]) < 0
	})
}

// удаляет строки, которых слов меньше columnNumber
func removeLinesWithWordsLessThan(lines []string, columnNumber int) []string {
	resStrs := make([]string, 0, len(lines))
	for i := range lines {
		splitedLine := strings.Split(strings.TrimSpace(lines[i]), " ")
		if len(splitedLine) >= columnNumber {
			resStrs = append(resStrs, lines[i])
		}
	}

	return resStrs
}

// считывает файл , разбивает по строкам, возращает строки
func readFileLines(filepath string) (lines []string, err error) {
	file, err := os.Open(filepath)
	if err != nil {
		return
	}
	reader := bufio.NewScanner(file)
	lines = []string{}
	for reader.Scan() {
		lines = append(lines, reader.Text())
	}
	return
}

// реверсия слайса
func reverseStringSlice(slice []string) {
	for i, j := 0, len(slice)-1; i < j; i, j = i+1, j-1 {
		slice[i], slice[j] = slice[j], slice[i]
	}
}

// сортировка строк по длинам
func sortStringsByNumeric(strs []string) {
	sort.Slice(strs, func(i, j int) bool {
		return len(strs[i]) > len(strs[j])
	})
}

// сортировка строк по длинам columnNumber словам
func sortLinesByColumnByNumeric(strs []string, columnNumber int) {
	sort.Slice(strs, func(i, j int) bool {
		words1 := strings.Split(strings.TrimSpace(strs[i]), " ")
		words2 := strings.Split(strings.TrimSpace(strs[j]), " ")

		if len(words1) < columnNumber && len(words2) < columnNumber {
			return len(strs[i]) < len(strs[j])
		} else if len(words1) < columnNumber {
			return true
		} else if len(words2) < columnNumber {
			return false
		}

		return len(words1[columnNumber-1]) < len(words2[columnNumber-1])
	})
}

// удаляет одиннаковые строки из слайса
func removeStringsDuplicates(strs []string) (resStrs []string) {
	mapBuffer := map[string]struct{}{}

	for i := range strs {
		mapBuffer[strs[i]] = struct{}{}
	}

	resStrs = make([]string, len(mapBuffer))

	i := 0
	for str := range mapBuffer {
		resStrs[i] = str
		i++
	}

	return
}
