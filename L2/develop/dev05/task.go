package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"regexp"
	"strings"
)

// Parameters содержит состояние всех возможных аргументов
type Parameters struct {
	A int  // "after" печатать +N строк после совпадения
	B int  // "before" печатать +N строк до совпадения
	C int  // "context" (A+B) печатать ±N строк вокруг совпадения
	c bool // "count" (количество строк)
	i bool // "ignore-case" (игнорировать регистр)
	v bool // "invert" (вместо совпадения, исключать)
	F bool // "fixed", точное совпадение со строкой, не паттерн
	n bool // "line num", напечатать номер строки
}

// Grep содержит параметры и шаблон строковый, по котрому будет осуществлен поиск
type Grep struct {
	parameters Parameters
	patterns   string
}

// NewGrep ...
func NewGrep(p *Parameters, patterns string) *Grep {
	return &Grep{
		parameters: *p,
		patterns:   patterns,
	}
}

// Perform выполняет операцию поиска
func (g *Grep) Perform(files []*os.File) {
	// если игнорируем регистр
	if g.parameters.i {
		// приводим шаблон поиск к нижнему регистру
		g.patterns = strings.ToLower(g.patterns)
	}
	// заменяем специальные символы в шаблоне на обычные
	// нужно для того, чтобы regexp не учитывал их
	g.patterns = replaceSpecialSymbols(g.patterns)

	// количество (count)
	// если ищем количество совпадений ...
	if g.parameters.c {
		var count int
		var scanner *bufio.Scanner
		var text string
		var containsCount int

		// проходимся по каждому файлу
		for _, file := range files {
			scanner = bufio.NewScanner(file)
			count = 0
			// смотрим каждую строку в файле
			for scanner.Scan() {
				// если регистр не важен, то приводим строку к нижнему
				if g.parameters.i {
					text = strings.ToLower(scanner.Text())
				} else {
					text = scanner.Text()
				}
				if g.parameters.F {
					// если F, ищем точное совпадение
					containsCount = strings.Count(text, g.patterns)
				} else {
					// иначе ищем по шаблону
					doesContains, _ := regexp.MatchString(g.patterns, text)
					if doesContains {
						containsCount = 1
					} else {
						containsCount = 0
					}
				}
				if g.parameters.v && containsCount == 0 {
					count++
				} else if containsCount > 0 {
					count += containsCount
				}
			}
			if len(files) > 1 {
				fmt.Fprintf(os.Stdout, "%s: %d\n", file.Name(), count)
			} else {
				fmt.Fprintf(os.Stdout, "%d\n", count)
			}
		}
		// осуществляем поиск, выводим строки с совпадением
	} else {
		var text string
		var doesContains bool
		var stringsBefore []string
		var stringsAfter []string
		var fileLines []string
		var lastPrintedIndex int

		// смотрим каждую строку в файле
		for _, file := range files {
			// записываем строки файла с fileLines
			fileLines = readFileLines(file)
			// проходимся по каждой строке
			for i := range fileLines {
				// если регистр не важен, то приводим строку к нижнему
				if g.parameters.i {
					text = strings.ToLower(fileLines[i])
				} else {
					text = fileLines[i]
				}
				if parameters.F {
					// если F, ищем точное совпадение
					doesContains = strings.Contains(fileLines[i], g.patterns)
				} else {
					// иначе ищем по шаблону
					doesContains, _ = regexp.MatchString(g.patterns, text)
				}
				// если нужно искать строки без совпадений и совпадения не обнаружены
				if g.parameters.v && !doesContains {
					// если оставшихся строк меньше, чем нужно вывести "After"
					if g.parameters.A > len(fileLines)-i {
						// то берем строки до конца слайса
						stringsAfter = fileLines[i+1:]
					} else {
						// иначе берем g.parameters.A строк
						stringsAfter = fileLines[i+1 : i+g.parameters.A+1]
					}
					// то же самое для строк "Before", только в обратную сторону
					if g.parameters.B > i {
						stringsBefore = fileLines[:i]
					} else {
						stringsBefore = fileLines[i-g.parameters.B : i]
					}
					// каждую строку "Before"
					// печатаем в соотствии c количеством файлов, именем файла
					for j := range stringsBefore {
						if len(files) > 1 && g.parameters.n {
							fmt.Fprintf(os.Stdout, "%s:%d:%s\n", file.Name(), i-(g.parameters.B-j), stringsBefore[j])
						} else if len(files) > 1 && !g.parameters.n {
							fmt.Fprintf(os.Stdout, "%s:%s\n", file.Name(), stringsBefore[j])
						} else if len(files) == 1 && g.parameters.n {
							fmt.Fprintf(os.Stdout, "%d:%s\n", i-(g.parameters.B-j), stringsBefore[j])
						} else if len(files) == 1 && !g.parameters.n {
							fmt.Fprintf(os.Stdout, "%s\n", stringsBefore[j])
						}
					}
					// печатаем саму строку подходящую
					if len(files) > 1 && g.parameters.n {
						fmt.Fprintf(os.Stdout, "%s:%d:%s\n", file.Name(), i+1, fileLines[i])
					} else if len(files) > 1 && !g.parameters.n {
						fmt.Fprintf(os.Stdout, "%s:%s\n", file.Name(), fileLines[i])
					} else if len(files) == 1 && g.parameters.n {
						fmt.Fprintf(os.Stdout, "%d:%s\n", i+1, fileLines[i])
					} else if len(files) == 1 && !g.parameters.n {
						fmt.Fprintf(os.Stdout, "%s\n", fileLines[i])
					}

					// печатаем строки "After"
					lastPrintedIndex = i
					for j := range stringsAfter {
						if len(files) > 1 && g.parameters.n {
							fmt.Fprintf(os.Stdout, "%s:%d:%s\n", file.Name(), i-(g.parameters.B-j), stringsAfter[j])
						} else if len(files) > 1 && !g.parameters.n {
							fmt.Fprintf(os.Stdout, "%s:%s", file.Name(), stringsAfter[j])
						} else if len(files) == 1 && g.parameters.n {
							fmt.Fprintf(os.Stdout, "%d:%s\n", i-(g.parameters.B-j), stringsAfter[j])
						} else if len(files) == 1 && !g.parameters.n {
							fmt.Fprintf(os.Stdout, "%s\n", stringsAfter[j])
						}
						lastPrintedIndex++
					}
					// иначе если ищем по совпадению и оно найдено
				} else if !g.parameters.v && doesContains {
					if g.parameters.A > len(fileLines)-i {
						stringsAfter = fileLines[i+1:]
					} else {
						stringsAfter = fileLines[i+1 : i+g.parameters.A+1]
					}
					if g.parameters.B > i {
						stringsBefore = fileLines[:i]
					} else {
						stringsBefore = fileLines[i-g.parameters.B : i]
					}
					for j := range stringsBefore {
						if len(files) > 1 && g.parameters.n {
							fmt.Fprintf(os.Stdout, "%s:%d:%s\n", file.Name(), i-(g.parameters.B-j)+1, stringsBefore[j])
						} else if len(files) > 1 && !g.parameters.n {
							fmt.Fprintf(os.Stdout, "%s:%s\n", file.Name(), stringsBefore[j])
						} else if len(files) == 1 && g.parameters.n {
							fmt.Fprintf(os.Stdout, "%d:%s\n", i-(g.parameters.B-j)+1, stringsBefore[j])
						} else if len(files) == 1 && !g.parameters.n {
							fmt.Fprintf(os.Stdout, "%s\n", stringsBefore[j])
						}
					}
					if len(files) > 1 && g.parameters.n {
						fmt.Fprintf(os.Stdout, "%s:%d:%s\n", file.Name(), i+1, fileLines[i])
					} else if len(files) > 1 && !g.parameters.n {
						fmt.Fprintf(os.Stdout, "%s:%s\n", file.Name(), fileLines[i])
					} else if len(files) == 1 && g.parameters.n {
						fmt.Fprintf(os.Stdout, "%d:%s\n", i+1, fileLines[i])
					} else if len(files) == 1 && !g.parameters.n {
						fmt.Fprintf(os.Stdout, "%s\n", fileLines[i])
					}
					lastPrintedIndex = i
					for j := range stringsAfter {
						if len(files) > 1 && g.parameters.n {
							fmt.Fprintf(os.Stdout, "%s:%d:%s\n", file.Name(), i+j+2, stringsAfter[j])
						} else if len(files) > 1 && !g.parameters.n {
							fmt.Fprintf(os.Stdout, "%s:%s", file.Name(), stringsAfter[j])
						} else if len(files) == 1 && g.parameters.n {
							fmt.Fprintf(os.Stdout, "%d:%s\n", i+j+2, stringsAfter[j])
						} else if len(files) == 1 && !g.parameters.n {
							fmt.Fprintf(os.Stdout, "%s\n", stringsAfter[j])
						}
						lastPrintedIndex++
					}
					fmt.Fprintln(os.Stdout, "--")
				}
			}

		}
	}
}

// считывает строки файла и возвращает
func readFileLines(file *os.File) (lines []string) {
	lines = []string{}
	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	return
}

var parameters = Parameters{
	A: 0,
	B: 0,
	C: 0,
	c: false,
	i: false,
	v: false,
	F: false,
	n: false,
}

// инициализируем аргументы
func init() {
	flag.IntVar(&parameters.A, "A", 0, "печатать +N строк после совпадения")
	flag.IntVar(&parameters.B, "B", 0, "печатать -N строк до совпадения")
	flag.IntVar(&parameters.C, "C", 0, "печатать ±N строк вокруг совпадения")
	flag.BoolVar(&parameters.c, "c", false, "количество строк")
	flag.BoolVar(&parameters.i, "i", false, "игнорировать регистр")
	flag.BoolVar(&parameters.v, "v", false, "вместо совпадения, исключать")
	flag.BoolVar(&parameters.F, "F", false, "точное совпадение, не паттерн")
	flag.BoolVar(&parameters.n, "n", false, "напечатать номер строки")
}

func main() {
	flag.Parse()

	if parameters.C > parameters.A {
		parameters.A = parameters.C
	}
	if parameters.C > parameters.B {
		parameters.B = parameters.C
	}

	if err := checkParametersValidation(); err != nil {
		panic(err)
	} else if len(flag.Args()) == 0 {
		panic("не введен шаблон")
	}

	patterns := flag.Args()[0]
	grep := NewGrep(&parameters, patterns)
	files := []*os.File{os.Stdin}
	if filesNumber := len(flag.Args()[1:]); filesNumber > 0 {
		files = make([]*os.File, 0, filesNumber)
		for _, filepath := range flag.Args()[1:] {
			file, err := os.Open(filepath)
			if err != nil {
				fmt.Fprintln(os.Stderr, err)
				continue
			}
			files = append(files, file)
		}
	}

	grep.Perform(files)
	for i := range files {
		files[i].Close()
	}
}

// проверяет корректность аргументов
func checkParametersValidation() (err error) {
	switch {
	case parameters.C < 0:
		err = fmt.Errorf("%s", "C < 0")
	case parameters.A < 0:
		err = fmt.Errorf("%s", "A < 0")
	case parameters.B < 0:
		err = fmt.Errorf("%s", "B < 0")
	}

	return
}

// заменяет специальные символы на обычные
func replaceSpecialSymbols(str string) string {
	rns := []rune(str)
	res := make([]rune, 0, len(rns)*2)

	for _, rn := range rns {
		if isSpecialSymbol(rn) {
			res = append(res, '\\')
		}
		res = append(res, rn)
	}

	return string(res)
}

// проверяет, является ли символ специальным
func isSpecialSymbol(rn rune) bool {
	return rn == '?' || rn == '+' || rn == '{' || rn == '|' || rn == '(' || rn == ')'
}
