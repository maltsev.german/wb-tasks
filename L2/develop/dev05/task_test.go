package main

import (
	"fmt"
	"regexp"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestRegexp(t *testing.T) {
	// reg := regexp.
	isTrue, err := regexp.MatchString(`\?hello`, " ?hello")
	if err != nil {
		fmt.Println(err.Error())
	}
	assert.True(t, isTrue)

	isTrue, _ = regexp.MatchString("func", "func string")
	assert.True(t, isTrue)
}

func TestReplaceSpecialSymbols(t *testing.T) {
	r := replaceSpecialSymbols("?+hello")
	assert.Equal(t, `\?\+hello`, r)

	isTrue,_ := regexp.MatchString("?+hello", "?+hello")
	assert.False(t, isTrue)

	isTrue, _ = regexp.MatchString(r, "?+hello")
	assert.True(t, isTrue)
}