package main

import "log"

type IDebugState interface {
	SetStateDisabled(v *Debug)
	SetStateEnabled(v *Debug)
	LogMessage(message string)
}

type Debug struct {
	state IDebugState
}

func (v *Debug) SetState(state IDebugState) {
	v.state = state
}

func (d *Debug) Enable() {
	d.state.SetStateEnabled(d)
}

func (d *Debug) Disable() {
	d.state.SetStateDisabled(d)
}

func (d *Debug) Log(message string) {
	d.state.LogMessage(message)
}

type DebugModeEnabledState struct {
	IDebugState
}

func (m *DebugModeEnabledState) SetStateDisabled(v *Debug) {
	v.SetState(&DebugModeDisabledState{})
}

func (m *DebugModeEnabledState) SetStateEnabled(v *Debug) {}

func (m *DebugModeEnabledState) LogMessage(message string) {
	log.Println(message)
}

type DebugModeDisabledState struct {
	IDebugState
}

func (m *DebugModeDisabledState) SetStateDisabled(v *Debug) {}

func (m *DebugModeDisabledState) SetStateEnabled(v *Debug) {
	v.SetState(&DebugModeEnabledState{})
}

func (m *DebugModeDisabledState) LogMessage(message string) {}

func NewDebug() *Debug {
	return &Debug{
		state: &DebugModeDisabledState{},
	}
}

func main() {
	d := NewDebug()

	d.Log("Сообщение с дебагом по умолчанию")

	d.Disable()
	d.Log("Сообщение с выключенным дебагом")

	d.Enable()
	d.Log("Сообщение с включенным дебагом")
}
