package main

import "fmt"

type IVisitor interface {
	Make(ITelephone)
}

type PhoneTypeVisitor struct {
}

func (ptv *PhoneTypeVisitor) Make(tl ITelephone) {
	switch tl.(type) {
	case *Smartphone:
		fmt.Println("This is smartphone")
	case *OldPhone:
		fmt.Println("This is old phone")
	default:
		fmt.Println("Unknown phone type")
	}
}

type ITelephone interface {
	// PhoneType() string
}

type Smartphone struct {
	ITelephone
}

func (sp *Smartphone) Accept(visitor IVisitor) {
	visitor.Make(sp)
}

type OldPhone struct {
	ITelephone
}

func (op *OldPhone) Accept(visitor IVisitor) {
	visitor.Make(op)
}

type iPhone struct {
	Smartphone
}

func (iphone *iPhone) Accept(visitor IVisitor) {
	visitor.Make(iphone)
}

type UpdatePhoneTypeVisitor struct {
	PhoneTypeVisitor
}

func (uptv *UpdatePhoneTypeVisitor) Make(tp ITelephone) {
	switch tp.(type) {
	case *iPhone:
		fmt.Println("This is iPhone")
	default:
		uptv.PhoneTypeVisitor.Make(tp)
	}	
}

func main() {
	oldPhone := OldPhone{}
	smartphone := Smartphone{}

	phoneType := PhoneTypeVisitor{}

	oldPhone.Accept(&phoneType)
	smartphone.Accept(&phoneType)

	iphone := iPhone{}
	iphone.Accept(&phoneType)

	updatePhoneType := UpdatePhoneTypeVisitor{}

	oldPhone.Accept(&updatePhoneType)
	smartphone.Accept(&updatePhoneType)
	iphone.Accept(&updatePhoneType)

}
