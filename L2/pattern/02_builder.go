// Строитель 

package main

import (
	"fmt"
)

type HumanBuilder struct {
	name string
	age uint
}

func NewHumanBuilder () *HumanBuilder {
	return &HumanBuilder{}
}

func (hb *HumanBuilder) Name(name string) *HumanBuilder {
	hb.name = name
	return hb
}

func (hb *HumanBuilder) Age(age uint) *HumanBuilder {
	hb.age = age
	return hb
}

func (hb *HumanBuilder) Build() *Human {
	return &Human{
		Name: hb.name,
		Age: hb.age,
	}
}

type Human struct {
	Name string
	Age uint
}

func main() {
	human := NewHumanBuilder().Name("Vasya").Age(15).Build()
	fmt.Println(human)
}
