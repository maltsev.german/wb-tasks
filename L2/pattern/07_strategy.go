package main

import (
	"fmt"
)

const (
	roadStrategy = iota
	pulbicTransportStrategy
	walkingStrategy
)

type IRouteStrategy interface {
	BuildRoute() string
}

type RoadStrategy struct {
	IRouteStrategy
}

func (r *RoadStrategy) BuildRoute() string {
	return "Стратегия дороги"
}

type PulbicTransportStrategy struct {
	IRouteStrategy
}

func (r *PulbicTransportStrategy) BuildRoute() string {
	return "Стратегия общественного транспорта"
}

type WalkingStrategy struct {
	IRouteStrategy
}

func (r *WalkingStrategy) BuildRoute() string {
	return "Стратегия гулящего"
}

type Novigator struct {
	routeStrategy IRouteStrategy
}

func (rs *Novigator) SetStrategy(strategy int) error {
	switch strategy {
	case roadStrategy:
		rs.routeStrategy = &RoadStrategy{}
	case pulbicTransportStrategy:
		rs.routeStrategy = &PulbicTransportStrategy{}
	case walkingStrategy:
		rs.routeStrategy = &WalkingStrategy{}
	default:
		return fmt.Errorf("невалидная стратегия построения маршрута")
	}

	return nil
}

func (nov *Novigator) Execute() string {
	return nov.routeStrategy.BuildRoute()
}

func main() {
	nov := Novigator{}
	var strategy int

	fmt.Println("Введите номер стратегии")
	fmt.Scan(&strategy)
	if err := nov.SetStrategy(strategy); err != nil {
		panic(err)
	}

	fmt.Println(nov.Execute())
}
