package main

import "fmt"

// Acount ...
type Account struct {
	Balance int
}

// IOperation ...
type IOperation interface {
	Execute()
}

type Deposit struct {
	IOperation
	IsComplete bool
	account    *Account
	money      int
}

// NewDeposit ...
func NewDeposit(account *Account, money int) *Deposit {
	return &Deposit{
		IsComplete: false,
		account:    account,
		money:      money,
	}
}

func (dep *Deposit) Execute() {
	dep.account.Balance += dep.money
	dep.IsComplete = true
}

type Cashback struct {
	IOperation
	IsComplete bool
	account    *Account
	money      int
	cashback    int
}

func NewCashback(account *Account, money, cashback int) *Cashback {
	return &Cashback{
		IsComplete: false,
		account:    account,
		money:      money,
		cashback: cashback,
	}
}

func (cb *Cashback) Execute() {
	cb.account.Balance += cb.money / 100 * cb.cashback
	cb.IsComplete = true
}

func main() {
	acc := Account{}

	fmt.Println(acc)

	dep := NewDeposit(&acc, 100)

	fmt.Println(acc)

	dep.Execute()

	fmt.Println(acc)

	NewCashback(&acc, 100, 50).Execute()

	fmt.Println(acc)
}
