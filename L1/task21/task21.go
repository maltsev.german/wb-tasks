package main

import "fmt"

type Hello interface {
	SayHello()
}

type Dog struct {
	Alias string
}

type Human struct {
	Hello
	Name string
}

func (h *Human) SayHello() {
	fmt.Println("Hello from Human, I am ", h.Name)
}

// Метод 1
type AdapterForDog struct {
	Hello
	Dog
}

func (afd *AdapterForDog) SayHello() {
	fmt.Println("Hello from Dog, I am ", afd.Alias)
}
//

// Метод 2
// Просто реализуем метод SayHello из интерфейса Hello, чтобы удовлетворять ему
func (d *Dog) SayHello() {
	fmt.Println("Hello from Dog, I am ", d.Alias)
}

func fSayHello(obj Hello) {
	obj.SayHello()
}
//

func main() {
	h := Human{Name: "Bobby"}
	fSayHello(&h)

	d := Dog{Alias: "Doggy"}
	fSayHello(&d)

	a := AdapterForDog{Dog: d}
	fSayHello(&a)
}
