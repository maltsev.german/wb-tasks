package main

import "fmt"

var justString string

// своя реализации функции createHugeString,
// возращает строку с stringSize букв 'a'
func createHugeString(stringSize int64) string {
	res := []byte{}

	for i := int64(0); i < stringSize; i++ {
		res = append(res, []byte("a")...)
	}

	return string(res)
}

func someFunc() {
	// в реализации функции someFunc
	// justString, являющейся строкой, присваивался слайс
	// v[:100], где v - возможно, типа string
	// индексация v для получения подстроки подходила только в том случае,
	// если v - ASCII-строка, если бы v была utf-8-строкой, индексация могла бы привести к тому,
	// что в конец justString была бы записана лишь часть какого-либо символа

	// сперва приводим результат createHugeString к []rune
	v := []rune(createHugeString(1 << 10))
	// индексируемся по []rune
	justString = string(v[:100])
}

func main() {
	someFunc()
	fmt.Println(justString)
}
