package main

import (
	"fmt"
)


type Action struct{}

// Пара функций структуры Action,
// которые будут встроены в Human
func (a *Action) Sing() {
	fmt.Println("I am singing")
}

func (a *Action) Dance() {
	fmt.Println("I am dancing")
}
//

type Human struct {
	Action // Встраиваем функции из Action в структуру Human
	Age        int
	Name       string
	Profession string
}

func main() {
	h := Human{}
	// Проверяем, что методы встроились в Human и доступны для вызова
	h.Sing()
	h.Dance()
}