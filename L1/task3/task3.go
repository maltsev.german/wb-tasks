package main

import (
	"fmt"
	"sync"
)

func main() {
	wg := sync.WaitGroup{}
	mutex := sync.Mutex{}
	nums := []int{2, 4, 6, 8, 10}
	sqrtNums := map[int]int{}
	sum := 0

	for _, num := range nums {
		// Указываем, что ждать на одну горутину больше
		wg.Add(1)

		// запуск новой горутины для нахождения квадрата отдельного числа
		// результат вычисления будет занесен в мапу sqrtNums
		go func(num int, m *map[int]int, mutex *sync.Mutex) {
			square := num * num

			// Блокируем операции изменения мапы, чтобы избежать гонку данных
			mutex.Lock()
			(*m)[num] = square
			// Разблокировка
			mutex.Unlock()
			wg.Done()
		}(num, &sqrtNums, &mutex)
	}

	// ждем завершения всех горутин
	wg.Wait()

	// Сумируем все квадраты
	for _, num := range nums {
		sum += sqrtNums[num]
	}
	fmt.Println(sum)
}
