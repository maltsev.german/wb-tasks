package main

import (
	"fmt"
	"math/rand"
	"sort"
)

func main() {
	sl := []int{}

	for i := 0; i < 20; i++ {
		sl = append(sl, rand.Int()%100)
	}

	// сперва сортируем слайс
	sort.Ints(sl)
	fmt.Println("Slice: ", sl)
	numToFind := sl[10]
	fmt.Println("Num to find:\t", numToFind)

	// бинырный поиск значения в массиве/слайсе
	index := sort.SearchInts(sl, numToFind)
	fmt.Println("After:\t", index)

}
