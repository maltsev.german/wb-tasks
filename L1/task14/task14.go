package main

import (
	"fmt"
)

// дает ответ, параметр какого типа был передан
// в функцию
func whatTheType(val interface{}) (res string) {
	switch val.(type) {
	case int:
		res = "int"
	case string:
		res = "string"
	case bool:
		res = "bool"
	default:
		res = fmt.Sprintf("%T", val)
		if res[:4] == "chan" {
			res = "channel"
		}
	}
	return
}

func main() {
	var v interface{} = 32
	fmt.Println(whatTheType(v))

	v = "32"
	fmt.Println(whatTheType(v))

	v = false
	fmt.Println(whatTheType(v))

	v = make(chan interface{})
	fmt.Println(whatTheType(v))
}
