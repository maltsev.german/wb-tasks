package main

import (
	"fmt"
	"time"
)

// выполняет пустой цикл, пока
// время не достигнет нужного значения
func mySleep(d time.Duration) {
	waitTill := time.Now().Add(d)
	for time.Now().Before(waitTill) {
	}
}

func main() {
	mySleep(time.Second * 5)
	fmt.Println("Waiting finished")
}
